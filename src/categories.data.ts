import { Category } from './app/interfaces/categories.interface';

export const Categories: Category[] = [
    {   
        name : 'Arte',
        description:'Los talleres artísticos son actividades extracurriculares de participación libre y voluntaria. Su finalidad es contribuir a la formación integral del estudiante desde la experiencia de conocimiento, apreciación y creación artística, a través de la búsqueda personal y comprometida de los participantes.',
        image:'https://i2.wp.com/www.centrosophia.com/wp-content/uploads/2018/02/girl-2696947_1280.jpg?fit=900%2C900&ssl=1'
    },
    {
        name : 'Musica',
        description:' El curso taller de música es un curso de especialidad cuyo objetivo es desarrollar la capacidad de analizar y comprender diferentes obras musicales según sus elementos formales. ... Música, cuerpo, emoción y acción. Interpretación musical básica en canto y en por lo menos un instrumento.',
        image:'https://previews.123rf.com/images/abstract412/abstract4121710/abstract412171000005/87671626-dise%C3%B1o-de-carteles-de-m%C3%BAsica-con-notas-musicales-notas-musicales-coloridas-ilustraci%C3%B3n-vectorial-aislado.jpg'
    },
    {
        name : 'Idiomas',
        description:'Descubre los cursos de idiomas que se ofrecen. Escoge el curso de idiomas que se adapte a tus necesidades.',
        image:'https://touridiomas.com/site/wp-content/uploads/idiomas-capa-3.png'
    },
    {
        name : "Deportes",
        description:'Los deportes son una actividad muy entretenida, que además de mantenernos con la mente ocupada, nos ayuda en nuestra salud física, por lo que son un balance entra salud y bienestar emocional. Existen una gran cantidad de deportes hoy en día, y siempre es bueno saber de alguno, aunque sean sus aspectos más básicos.',
        image:'https://s.france24.com/media/display/6aca8d1a-7783-11ea-9cf2-005056bf87d6/w:1280/p:16x9/WEB%2005ABR%20DEPORTES%20PORTADA%20FOTO.jpg'
    },
    {
        name : "Baile",
        description:'La danza es una fuente de expresión, de transmisión de sentimientos, sensaciones y emociones. La actividad dancística ayuda además al conocimiento de su propio cuerpo, de sus extremidades, de las posibilidades de movimiento de cada parte que conforma su anatomía y de este modo mantener un mejor estado físico general.',
        image:'https://www.diariodecuyo.com.ar/export/sites/diariodecuyo/img/2019/10/21/challenge.jpg_28340506.jpg'
    },
    {
        name : "Otro",
        description:'Talleres que no forman parte de nuestras categorias podras encontrarlos en este apartado.',
        image:'https://img.sorianoticias.com/imagenes/2019-08/jovenes-amigos-parque_53876-46877.jpg'
    }
]