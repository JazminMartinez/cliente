import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth-service.service';
import { User } from 'src/app/interfaces/user.interface';
import { WorkshopServiceService } from 'src/app/services/workshop-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  
  //Crea una variable para almacenar los talleres mas recientes
  mostRecentWorkshop:any;
  isAuthenticated: boolean;
  user:User;
  //esta varible booleana permite mostrar un mensaje cuando no hay talleres
  showNotice:boolean=false;
  constructor(private router: ActivatedRoute,private auth:AuthService,private wss:WorkshopServiceService) { }

  ngOnInit(): void {
    //verificamos si el usuario ha iniciado sesion para poder ver los componentes
    this.auth.isAutheticated.subscribe(credetials => {
      this.isAuthenticated = credetials;
    });
    this.auth.verifyAuth().subscribe(resp => {});

    //se suscribe al observable y asigna la data a la variable mostRecentWorkshop
     this.router.data.subscribe(data => {
       this.mostRecentWorkshop=data.workshops;
       console.log("talleres recientes: ",this.mostRecentWorkshop.length)
       if(this.mostRecentWorkshop.length===0){
         this.showNotice=true;
       }
     });
    
    // this.wss.getMostRecentWorkshops().subscribe(data=>{
    //   console.log("datos del servicio workshop: ",data);
    //    this.mostRecentWorkshop=data;
    // //   console.log("talleres recientes: ",this.mostRecentWorkshop.length)
    //    if(this.mostRecentWorkshop.length===0){
    //      this.showNotice=true;
    //    }
    // })
  }
}
