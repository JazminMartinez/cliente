import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-there-is-not',
  templateUrl: './there-is-not.component.html',
  styles: [
  ]
})
export class ThereIsNotComponent implements OnInit {
  @Input() mensaje:string;
  constructor() { }

  ngOnInit(): void {
  }

}
