import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styles: [
  ]
})
export class SubscribeComponent implements OnInit {
  @Input() mensaje:string;
  @Output() cerrarModal = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }

  onClose() {
    this.cerrarModal.emit(false);
  }

}
