import { Component, OnInit, Output,EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styles: [
  ]
})
export class ModalComponent implements OnInit {
  @Output() cerrarModal = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }
  onClose() {
    this.cerrarModal.emit(false);
  }



}
