import { Component, OnInit } from '@angular/core';
import { Workshop } from 'src/app/interfaces/workshop.interface';
import { User } from 'src/app/interfaces/user.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkshopServiceService } from 'src/app/services/workshop-service.service';

@Component({
  selector: 'app-workshops-user',
  templateUrl: './workshops-user.component.html',
  styles: [
  ]
})
export class WorkshopsUserComponent implements OnInit {
  user:User;
  student=false;
  workshops:Workshop[];
  nohay=false;
  mensaje='';
  constructor(private router: ActivatedRoute,private wss:WorkshopServiceService,private router2:Router) { }

  ngOnInit(): void {
    this.router.data.subscribe(data => {
      
      //checa si el tamaño del array de horarios es 0 y envia un mensaje de que no hay horarios
      this.workshops=data.workshops;
      if(this.workshops.length===0){
        this.nohay=true;
        this.mensaje="Actualmente no hay talleres por mostrar.";
      }else{
        this.nohay=false
      }
      
      //valida que si el rol es de un estudiante, para poder mostrar un elemento u ocultarlo
      this.user=data.user.user;
      if(this.user.role==='student'){
        this.student=true;
      }else{
        this.student=false;
      }
    });
  }
}