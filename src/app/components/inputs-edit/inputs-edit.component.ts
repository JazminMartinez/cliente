import { Component, OnInit,Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-inputs-edit',
  templateUrl: './inputs-edit.component.html',
  styles: [
  ]
})
export class InputsEditComponent implements OnInit {
  @Input() label:string;
  @Input() inputFormControl: FormControl;
  @Input() type:string;
  @Input() value:any;
  @Input() mensaje:string;
  constructor() { }

  ngOnInit(): void {
    this.inputFormControl.setValue(this.value);
  }

  get isInvalid(){
    return this.inputFormControl.touched && this.inputFormControl.dirty && this.inputFormControl.errors;
  }

 get isRequiredInvalid(){
   return this.inputFormControl.errors.required;
 }
 get isPatternInvalid(){
   return this.inputFormControl.errors.pattern;
 }

}
