import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-workshops-bc',
  templateUrl: './workshops-bc.component.html',
  styles: [
  ]
})
export class WorkshopsBCComponent implements OnInit {
  workshopsByCategory:any;
  tam:number;
  show:boolean=true;

  constructor(private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.router.data.subscribe(data => {
      this.workshopsByCategory = data.workshops;
      this.tam=this.workshopsByCategory.length
      if(this.tam>0){
        this.show=false;
      }
    });
  }
  cerrarModal(value:boolean){
    this.show=value;
  }

}
