import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/interfaces/categories.interface';
import { Categories } from 'src/categories.data';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styles: [
  ]
})
export class CategoriesComponent implements OnInit {
  categorias:Category[]=Categories;
  constructor() { }

  ngOnInit(): void {
  }

}
