import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { WorkshopServiceService } from '../services/workshop-service.service';

@Injectable({
  providedIn: 'root'
})
export class WorkshopsUserResolverService implements Resolve<any>{

  constructor(private wss: WorkshopServiceService) { }

  resolve(router:ActivatedRouteSnapshot, state:RouterStateSnapshot):any | Observable<any>{
    return this.wss.getWorkshopsUser();
   }
}
